pref("extensions.jid1-KtlZuoiikVfFew@jetpack.whitelist", "");
pref("extensions.jid1-KtlZuoiikVfFew@jetpack.complaint_tab", true);
pref("extensions.jid1-KtlZuoiikVfFew@jetpack.display_notifications", false);
pref("extensions.jid1-KtlZuoiikVfFew@jetpack.complaint_email_subject", "Please make your JavaScript free");
pref("extensions.jid1-KtlZuoiikVfFew@jetpack.complaint_email_body", "I could not use your site because it requires running JavaScript code which is not free software.  Since a nonfree program disrespects the user's freedom, I decided not to run it. \n\nSee http://gnu.org/philosophy/javascript-trap.html for more information, and please make your JavaScript code free.");
